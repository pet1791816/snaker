import './style.css';
import Game from './classes/Game';

const canvas = document.getElementById('app') as HTMLCanvasElement;

new Game(canvas);
