import { Drawable } from "./Cell";
import SnakeCell from "./SnakeCell";

const enum Direction {
    BOTTOM,
    LEFT,
    TOP,
    RIGHT
}

const directions: { [key in Direction]: string } = {
    [Direction.BOTTOM]: 'ver',
    [Direction.LEFT]: 'hor',
    [Direction.TOP]: 'ver',
    [Direction.RIGHT]: 'hor',
}

export default class Snake extends Drawable {
    private body: SnakeCell[];
    private direction: Direction = Direction.BOTTOM

    constructor() {
        super();
        this.body = [
            new SnakeCell(5, 5),
            new SnakeCell(5, 6),
            new SnakeCell(5, 7),
            new SnakeCell(5, 8)
        ];

        this.initEvents();
    }

    draw(ctx: CanvasRenderingContext2D) {
        for(let cell of this.body) {
            cell.draw(ctx)
        }
    }

    move() {
        const firstCell = this.body[this.body.length - 1];
        let newCell: SnakeCell;

        switch(this.direction) {
            case Direction.BOTTOM:
                newCell = new SnakeCell(firstCell.x, firstCell.y + 1);
                break;
            case Direction.LEFT:
                newCell = new SnakeCell(firstCell.x - 1, firstCell.y);
                break;
            case Direction.TOP:
                newCell = new SnakeCell(firstCell.x, firstCell.y - 1);
                break;
            case Direction.RIGHT:
                newCell = new SnakeCell(firstCell.x + 1, firstCell.y);
                break;
            default:
                const exhaustiveCheck: never = this.direction;
                throw new Error(exhaustiveCheck);    
        }

        this.body.shift();
        this.body.push(newCell);
    }

    initEvents() {
        document.addEventListener('keydown', event => {
            let newDirection: Direction = this.direction;

            if(event.key === 'ArrowDown') newDirection = Direction.BOTTOM;
            else if(event.key === 'ArrowLeft') newDirection = Direction.LEFT;
            else if(event.key === 'ArrowUp') newDirection = Direction.TOP;
            else if(event.key === 'ArrowRight') newDirection = Direction.RIGHT;
            
            if(directions[this.direction] === directions[newDirection]) return;
            this.direction = newDirection;
        })
    }

    getHead() {
        return this.body.at(-1);
    }

    getBody() {
        return this.body;
    }

    add(x: number, y: number) {
        this.body.push(new SnakeCell(x, y))
    }
}