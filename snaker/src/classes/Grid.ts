import { theme } from '../theme';
import { Drawable } from './Cell';
import EmptyCell from './EmptyCell'


export default class Grid {
    
    private grid: any[][] = [];
    private canvas: HTMLCanvasElement;
    private objects: Drawable[] = [];

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
    }

    public setObjects(objects: Drawable[]) {
        this.objects = objects;
    }

    public render() {
        const ctx = this.canvas.getContext('2d')!;
        const cellSize = theme.sizes.cell;
        
        const columns = Math.ceil(window.innerWidth / cellSize);
        const rows = Math.ceil(window.innerHeight / cellSize);

        for(let x = 0; x <= columns; x++) {
            for(let y = 0; y <= rows; y++) {
                if(!this.grid[x]?.[y]) {
                    const emptyCell = new EmptyCell(x, y);
                    emptyCell.draw(ctx);  
                }
            }
        }
        for(let object of this.objects) {
            object.draw(ctx);
        }
    }
}