import { theme } from '../theme';
import FoodCell from './FoodCell';
import Grid from './Grid';
import Snake from './Snake';

const startFood = [new FoodCell(10, 10), new FoodCell(20, 21), new FoodCell(20, 22)];

export default class Game {
    private grid!: Grid;
    private snake!: Snake;
    private timer: number = -Infinity;
    private canvas: HTMLCanvasElement;
    private food!: FoodCell[];

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
        this.start();
    }

    private start() {
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        this.food = [...startFood];

        this.snake = new Snake();

        this.grid = new Grid(this.canvas);
        this.grid.setObjects([
            this.snake,
            ...[...this.food]
        ]);
        
        this.animate();
    }

    private animate() {
        if((performance.now() - this.timer) > theme.times.step) {
            this.timer = performance.now();
            this.grid.render();
            this.snake.move();
            this.checkCollisions();
        }
        requestAnimationFrame(this.animate.bind(this));
    }

    private checkCollisions() {
        const head = this.snake.getHead()!;
        const body = this.snake.getBody()!;

        for(let i = 0; i < body.length; i++) {
            if(body[i] === head) continue;
            if(body[i].x === head.x && body[i].y === head.y ) {
                this.start();
            }
        }

        for(let i = 0; i < this.food.length; i++) {
            if(this.food[i].x === head.x && this.food[i].y === head.y ) {
                this.snake.add(this.food[i].x, this.food[i].y);
                this.food.splice(i, 1);
                this.grid.setObjects([
                    this.snake,
                    ...this.food
                ])
            }
        }

    }
}