

export abstract class Drawable {
    public draw(_ctx: CanvasRenderingContext2D) {
        console.error('need realize cell draw')
    };
}

export default abstract class Cell extends Drawable{
    public x: number;
    public y: number;
    constructor(x: number, y: number) {
        super();
        this.x = x;
        this.y = y;
    }
    
}