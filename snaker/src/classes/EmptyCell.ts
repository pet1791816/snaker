import { theme } from '../theme';
import Cell from './Cell'

export default class EmptyCell extends Cell {
    public draw(ctx: CanvasRenderingContext2D) {
        const cellSize = theme.sizes.cell;
        ctx.fillStyle = theme.colors.empty;
        //ctx.strokeRect(this.x * cellSize, this.y * cellSize, cellSize, cellSize)
        ctx.fillRect(this.x * cellSize, this.y * cellSize, cellSize, cellSize)
    }
}