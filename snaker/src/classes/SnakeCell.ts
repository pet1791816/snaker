import { theme } from '../theme';
import Cell from './Cell'

export default class SnakeCell extends Cell {
    public draw(ctx: CanvasRenderingContext2D) {
        const cellSize = theme.sizes.cell;
        ctx.fillStyle = theme.colors.snake;
        ctx.fillRect(this.x * cellSize, this.y * cellSize, cellSize, cellSize)
    }
}