import { theme } from '../theme';
import Cell from './Cell'

export default class FoodCell extends Cell {
    public draw(ctx: CanvasRenderingContext2D) {
        const cellSize = theme.sizes.cell;
        ctx.fillStyle = theme.colors.food;
        ctx.beginPath();
        ctx.arc(
            this.x * cellSize + cellSize / 2, 
            this.y * cellSize + cellSize / 2, 
            cellSize / 2,
            0, 
            2 * Math.PI)
        ctx.fill();
    }
}