export const theme = {
    colors: {
        empty: '#e2ff6f',
        snake: '#ff6c70',
        food: '#ffa54a'
    },
    sizes: {
        cell: 10
    },
    times: {
        step: 100
    }
}